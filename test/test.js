print("//NOTE: just javascript file for the highlighting.\n\n//START")

state = new_state({ 'vars':{'code':run_code} })

var lst = []
for(var k in run_test_code) { lst.push(k) }

enter = [
    "[commentfeed,code]G..RR[works?];abcdef\n[non-ignored]",
//    "[o,cls]G..[commentfeed,cls]G.."
]
for( var i=0 ; i < enter.length ; i++ ) {
    print("//", i, 'enter:', enter[i])
    print(JSON.stringify(run(state, enter[i])))
    print()
}

print("// From files:")
lst.sort()
print(JSON.stringify(lst))
print()

for( var i=0 ; i<lst.length ; i++ ) {
    var code = run_test_code[lst[i]]
    print('//*', lst[i], code)
    print(JSON.stringify(run(state, code)))
    print()
}
