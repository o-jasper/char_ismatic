//NOTE: just javascript file for the highlighting.

//START
// 0 enter: [commentfeed,code]G..RR[works?];abcdef
[non-ignored]
[{"tp":"B"},"works?","non-ignored"]

// From files:
["appendstr","factorial","fibonacci","list_stuff","modify","num_stuff","obj_stuff","run_nothing","square"]

//* appendstr [abc,def]A

[{"tp":"B"},"abcdef"]

//* factorial [F]{[x]^{1}{x1-Fx*}x2<?}[n]tsd  ; Define factorial onto number.
{1+F}6R

[{"tp":"B"},1,2,6,24,120,720]

//* fibonacci 
[F]{[n]^0  1{[x_]^x+xf}nR+}[n]tsd

{F}9R

[{"tp":"B"},1,2,3,5,8,13,21,34,55]

//* list_stuff [s]tn[n]tn[o]tn  ; Class instance creators.
[l]tn1fa2fa3fa4fa ; Add to lists.
[notme]1,2,3,4[l]tn{d[vl]^vla}4Rr ; Pack into list.(inefficiently)
[notme]1,2,3,4[l]tn{da}4Rr ; Pack into list manually.(append 4 times, reverse)
[notme]{1,2,3,4}[l]tnU          ; Shortcut to that function, & {} to count.
{1,2,3,4}[l]tnU{5,6,7,8}[l]tnUA ;  Pack into list twice, and append.

{[q]{1,2}[l]tnUi}4R

[{"tp":"B"},"",0,{},[1,2,3,4],"notme",[1,2,3,4],"notme",[1,2,3,4],"notme",[1,2,3,4],[1,2,3,4,5,6,7,8],["q",1,2],[1,"q",2],[1,2,"q"],[1,2,"q"]]

//* modify [modify,code]G..RR

{0,0}[o]tnU0,1M+

[{"tp":"B"},{"0":1}]

//* num_stuff 1,2,3
1234,24+

[{"tp":"B"},1,2,3,1258]

//* obj_stuff [o]tn1f2fs,3f4fs,5f4fsc ; Put stuff into dictionary & copy.
{1,2,3,4}[o]tn

[{"tp":"B"},{"1":2,"3":4,"5":4},{"tp":"C","d":-1,"s":"1,2,3,4","t":"{"},{}]

//* run_nothing []R

[{"tp":"B"},{"s":"","tp":"C"}]

//* square [S]{[x]^xx*}[n]tsd  ; Define square onto number.
2S3S

[{"tp":"B"},4,9]

