//  Copyright (C) 12-05-2019 Jasper den Ouden.
///
//  This is free software: you can redistribute it and/or modify
//  it under the terms of the Affero GNU General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

function code_string(code) {
    if(code.tp != 'C'){
        console.error("Trying to range over indices with a non-body", code)
    }
    if( code.s === undefined ){ console.error("missing code", code) }
    return code.s
}

default_clses = {}  // These are defined in the `cls/` directory.

function prepare_cls(clses, cls_name, initial) {
    var cls = clses[cls_name]
    if(!cls){
        for( var k in clses) {
            if( clses[k].cls_short == cls_name ) {
                cls = clses[k]
                cls_name = k
                break
            }
        }
        if( !cls ) { console.error("Class missing", cls_name) }
    }

    if( cls.prepared ){ return }
    cls.prepared = true

    if( !cls['new'] && !cls['derives'] ) {  // Default constructor.
        cls['new'] = {'tp':'C', 's':"[o]tn"}
    }

    if( cls['derives'] ) {  // Link these up.
        prepare_cls(clses, cls['derives'], initial)
        cls['derives'] = clses[cls['derives']]
    }

    var set = {}  // Ensure stuff inside has types.
    for( var k in cls ) {
        var val = cls[k]
        if( ({'tp':1, 'as':1, 'cls_short':1,
              'no_val':1, 'derives':1, 'desc':1})[k] ) {
            continue
        } if(typeof val == 'object') {
            if( val[0] == 'm' ) {  // Involved function.
                val.tp = (typeof val[1]=='string' ? 'm' : 'mib')
            } else if( typeof val[0]=='number' ) {
                val.tp = 'ib'  // Inbuild function.
            } else if( val[0] == 'cm' ) {  // Keeps context.
                val.tp = 'cm'
            }
        } else if(typeof val == 'string') {
            if( !initial ){
                console.error("Dont expect bare strings after initial", k, val)
            }
            cls[k] = {'tp':'C', 's':val}
        }
    }  // Provide shortname version.
    cls.tp = 'c'  // Set type, name, ensure `new` creator.
    cls.cls_name = cls_name
    if( cls.cls_short ){ clses[cls.cls_short] = cls }
}

function cls_attr(cls, k) {  // Get attribute of class.
    while(cls) {
        var meth = cls[k]
        if(meth!=undefined){ return meth }
        // Note: somehow undefined and string undefined clash? (TODO ?)
        cls = cls.derives
    }
}

default_clses['class'] = {
    'derives' : 'o', 'cls_short' : 'c',
    '.' : [2, function(k, cls){ return [cls_attr(cls, k)] }],
  // Access and run creator, then check/fix type.
    'n' : "[c]^c[new]f.RcF",
    'F' : [2, function(obj, cls) {
        var tp = type(obj)
        if( tp != cls.cls_name && tp != cls.cls_short ) {
            obj.tp = cls.cls_short || cls.cls_name
        }
        return [obj]
    }],
  // Prepare a class for actual use.
    'p' : ['m', function(vars, chvals, clses, stack, code, i) {
        var cls = stack.pop()
        var name = stack.pop()
        vars.cls[name] = cls
        prepare_cls(vars.cls, name)
        return i
    }]
}

function list_append(lst, append) {
    while(append.length > 0) {
        lst.push(append.shift())
    }
}
function call_fun(n, fun, stack) {
    if( n==0 ) { list_append(stack, fun()) }
    else if( n==1 ) {
        list_append(stack, fun(stack.pop()))
    } else {
        var args = stack.splice(-n)
        switch(n) {
            case 2: list_append(stack, fun(args[0], args[1])); break
            case 3: list_append(stack, fun(args[0], args[1], args[2])); break
            case 4: list_append(stack, fun(args[0], args[1], args[2], args[3])); break
            default: list_append(stack, fun(args)); break
        }
    }
}

default_clses['inbuild_function'] = {
    'cls_short' : 'ib', 'derives':'B',
    'R' : ['m', function(vars, chvals, clses, stack, code, i) {
        var fun = stack.pop()
        call_fun(fun[0], fun[1], stack)
        return i
    }]
}

function type(val) {
    var tp = typeof(val)
    return tp=='object' && val.tp || tp
}

function raw_run(vars, chvals, clses, stack, code) {

    var i = 0, n = stack.length
    var consider_fun = function(cls, fun) {
        var tp = type(fun)
        switch(tp) {
            case 'ib':  // Inbuild.
                call_fun(fun[0], fun[1], stack)
                break
            case 'C':  // Runnable code.
                if( cls.as ){ top_val.tp = cls.as }
                if( fun.s!="" ) {
                    raw_run(vars, {'tp':'chvals'}, clses, stack, fun.s)
                }
                break
            case 'cm':
                // Run code WITH context.(like inserting code there.)
                if( cls.as ){ top_val.tp = cls.as }
                raw_run(vars, {'tp':'chvals', 'parent':chvals},
                        clses, stack, fun[1])
                break
            case 'm': // Something more elaborate.
                stack.push(code)
                stack.push(i)
                if( cls.as ){ top_val.tp = cls.as }
                raw_run(vars, {'tp':'chvals', 'parent':chvals},
                        clses, stack, fun[1])
                i = stack.pop()
                break
            case 'mib': // Ditto, but inbuild.
                i = fun[1](vars, chvals, clses, stack, code, i)
                break
        }
        i+=1
    }

    // console.log('CALL', !code, code, JSON.stringify(stack))
    while(i < code.length) {
        var ch = code[i]

        var top_val = stack[stack.length-1]
        var cls  = clses[type(top_val)]
        if( cls==undefined || !cls.no_val ) {
            var val = chvals[ch]  /// Not specified to look at values last.
            if(val!=undefined) {
                stack.push(val)
                i+=1
                continue
            }
        }

//        console.log(ch, type(top_val), '*', JSON.stringify(stack),
//             JSON.stringify(chvals))
        var fun = cls_attr(cls, ch)
        if(fun!=undefined) {  // A function/macro.
            consider_fun(cls, fun)
            continue
        }
        var fun = cls && cls['default']
        if(fun!=undefined) {
            stack.push(ch)  // This is the exception to the rule!
        // It runs with a character on top but not the method from that type.
            consider_fun(cls, fun)
            continue
        }
        console.log("-missing", type(top_val), "("+ch+"):",
                    code.substr(0,i)+">"+ch+"<"+code.substr(i+1),
                    cls_attr(cls, ch), code.length, stack)
        i++
    }
// If string runs out, call the exit function is available.
    var top_val = stack[stack.length-1]
    var cls  = clses[type(top_val)]
    if(cls && cls.exit) {
        consider_fun(cls, cls.exit)
    }
    return stack.length - n
}

function new_state(state) {
    state = state || {}
    state.vars     = state.vars || {}
    state.vars.cls = state.vars.cls || {}

    var clses = state.vars.cls
    for(var k in default_clses){ clses[k] = default_clses[k] }

    for(var cls_name in clses) {  // Prepare classes if not prepared yet.
        prepare_cls(clses, cls_name, true)
    }

    state.chvals = state.chvals || {}
    state.chvals.stacklens = state.chvals.stacklens || []
    state.clses = clses

    state.vars.tp   = 'o'
    state.chvals.tp = 'chvals'
    clses.tp  = 'o'

    state.default_stack = state.default_stack || [{'tp':'B'}]
    state.default_stack.tp = 'l'
    return state
}

function run(state, code, stack) {
    if(stack==null) {
        stack = []
        for( var i=0 ; i < state.default_stack.length ; i++ ) {
            stack.push(state.default_stack[i])
        }
    }
    raw_run(state.vars, state.chvals, state.clses, stack, code)
    return stack
}
