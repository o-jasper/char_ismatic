" Vim syntax file
" Language: char_ismatic
" Maintainer: Jasper den Ouden
" Latest Revision: 22 May 2019

" Put in ~/.vim/syntax/

if exists("b:current_syntax")
  finish
endif

syn match comment "\;.*$"
syn match stringfmt "[\[\]]"

" Regions
"syn region syntaxElementRegion start='\[' end='\]'
"syn region celDesc start="\[" end="\]" fold transparent


let b:current_syntax = "cel"

hi def link celTodo        Todo
hi def link comment        Comment
hi def link celBlockCmd    Statement
hi def link celHip         Type
hi def link stringfmt      Constant
hi def link celDesc        PreProc
hi def link celNumber      Constant
