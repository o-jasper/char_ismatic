
default_clses['object'] = {
    'derives' : 'bottom', 'cls_short' : 'o',
    'new' : [0, function(){ return [{}] }],
    'c' : "[o]tnA",  // Copy.

    '.' : [2, function(k, obj){ return [obj[k]]}],  // Use value as key.
    's' : [3, function(k, v, obj) {  // Set by potentially mon-character key.
        obj[k] = v
        return [obj]
    }],
    'A' : "f[x]^[[yk]^kx.kfys]xK",
    // Iterate the keys.
    'K' : ['m', function(vars, chvals, clses, stack, code, i) {
        var dict = stack.pop()
        var body = stack.pop()
        for( var k in dict ) {
            stack.push(k)
            raw_run(vars, chvals, clses, stack, body)
        }
        return i
    }],
    // Pack pairs. // NOTE odd number of values should indicate bug.
    'U' : ['cm', "[o]^Lpr{dosd}f2/Ro"],
}
