
default_clses['list'] = {
    'derives' : 'o', 'cls_short' : 'l',
    'new' : [0, function() { return [[]] }],
    '#' : [1, function(lst) { return [lst.length] }],
    'c' : "[l]tnA", // Make a copy.

    'r' : [1, function(lst) { return [lst.reverse()] }],

//    's' : [1, function(lst) { return [lst.shift()] }],
    'p' : [1, function(lst) { return [lst.pop()] }],

    'S' : [4, function(f,t, ins, lst) { lst.splice(f,t, ins); return [lst] }],
    'i' : "[.vl]^0vlS",

    'a' : [2, function(a, lst) { lst.push(a); return [lst] } ],  // Append.
    'A' : "[lt]^{Cl.tid}l#Rt",  // Append list.
    'u' : "[l]^{l.}l#R", // Unpack values.
// Pack values.
// in here `Lp` gets previous context, rest stuffs it into the list.
    'U' : ['cm', "[l]^Lpr{d0flid}fRl"],
//    '^' : ['cm', "[s.]^Us{^}LpR"], // Push into variables.

    'R' : ['cm', "[l]^{lf.LpR}l#R"],
}
