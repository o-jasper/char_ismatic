
default_clses['number'] = {
    'derives' : 'bottom', 'cls_short' : 'n',
    'new' : "0",
    '+' : [2, function(x,y) { return [x+y] }],
    '-' : [2, function(x,y) { return [x-y] }],
    '*' : [2, function(x,y) { return [x*y] }],
    '/' : [2, function(x,y) { return [x/y] }],
    '%' : [2, function(x,y) { return [x%y] }],
    '=' : [2, function(x,y) { return [x==y] }],
    '>' : [2, function(x,y) { return [x>y] }],
    '<' : [2, function(x,y) { return [x<y] }],

    '~' : [1, function(x) { return [~x] }],  // BITWISE operations.
    '|' : [2, function(x,y) { return [x|y] }],
//(it will differ if you use this as boolean, if you want boolean logic,
// use the boolean.
    '&' : [2, function(x,y) { return [x&y] }],

    's' : [1, function(x) { return [""+x] }],  // To string.
    '^' : [2, function(x,y) { return [Math.pow(x,y)] }],

    // Range of indices.
    'R' : ['m', function(vars, chvals, clses, stack, code, i) {
        var n = stack.pop()
        var body = code_string(stack.pop())
        for( var j=0 ; j<n ; j++ ) {
            stack.push(j)
            raw_run(vars, chvals, clses, stack, body)
        }
        return i
    }],
}
