
default_clses['code'] = {
    'derives' : 'o', 'cls_short':'C',
    'new' : "[s]f[o]tns",  // New from string.

    's' : "[s]f.",  // As string.

    // Run plainly.
    'r' : ['m', function(vars, chvals, clses, stack, code, i) {
        var run_code = code_string(stack.pop())
        stack.push(raw_run(vars, chvals, clses, stack, run_code))
        return i
    }],
    'R' : ['cm', "rd"],
    // While loop.
    'W' : ['m', function(vars, chvals, clses, stack, code, i) {
        var body = code_string(stack.pop())
        while(stack.pop()===true) {
            raw_run(vars, chvals, clses, stack, body)
        }
        return i
    }],
    //'C' : "[l]tnUr[l]^{[i]^{i2*1+l.R2B}{}i2*l.R?}l#2/R",
    // Expect n/2 condition,body pairs of string.
    'C' : ['m', function(vars, chvals, clses, stack, code, i) {  // Cond.
        var body = code_string(stack.pop())
        var lst = []
        var n = raw_run(vars, chvals, clses, stack, body)
        for( var j=0 ; j < n ; j++ ) { lst.append(stack.pop()) }

        for( var j=0 ; j <n ; j+=2 ) {
            raw_run(vars, chvals, clses, stack, lst[i])
            if( stack.pop()===true ) {
                raw_run(vars, chvals, clses, stack, lst[i+1], i)
                return i
            }
        }
        return i
    }],
}
