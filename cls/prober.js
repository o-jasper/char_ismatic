
default_clses['prober'] = {
    'cls_short' : 'P',
    'p' : [100, function(x){ x.pop(); console.log('probed', x); return x }],
    'P' : ['m', function(vars, chvals, clses, stack, code, i) {
        stack.pop()
        console.log('Probed',
                    {'chvals':chvals,'code':code, 'i':i, 'stack':stack})
        return i
    }],
}
