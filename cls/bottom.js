
default_clses['bottom'] = {
    'cls_short' : 'B',
    'L' : ['m', function(vars, chvals, clses, stack, code, i) {  // Local vars.
        if( chvals.tp != 'chvals' ){
            console.error("BUG local variables wrong type", code)
        }
        stack.push(chvals)
        return i
    }],
    'G' : ['m', function(vars, chvals, clses, stack, code, i) {  // Global.
        stack.push(vars)
        return i
    }],  // Dictionary of classes.
    't' : "[cls]G..",  // Dictionary of classes & immediately access."
    'T' : [1, function(val){ return [type(val)] }],

    // Start string feed.
    // NOTE: can't be make char_acteristic because no way to make the string
    //  to make the type the first time.
    '[' : [0, function(){ return [{'tp':'sf', 'd':0, 's':""}] }],
    'f' : [2, function(x,y){ return [y,x] }],
    'd' : [1, function(x){ return [] }],  // Drop one.
    // Copy one. (some objects have `c` for copying the contents)
    'C' : [1, function(x){ return [x,x] }],
    ',' : "", // nop, do nothing. (NOTE multiple in sequence is reserved)
    ' ' : "", // nop, NOTE, space-enclosed, not followed by comment`;` is reserved.
    '\n' : "",
    ';' :  "[cf]tn", // Make comment feed, which only stops with \n.
    '{' : [0, function(){ return [{'tp':'sf', 'd':0, 's':"", 't':'{'}] }],
//"[tp,sf][s,][t,\\{][o]tnsss",  // String feeder in code mode.
    'M' : "[M]tn",
}
{
    for( var d=0 ; d <10 ; d++ ) {  // Start out integers.
        default_clses['bottom']['' + d] = [0, function(d){
            return function(){ return [{'tp':'if', 'i':d}] }
        }(d)]
    }
}
