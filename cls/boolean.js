
default_clses['boolean'] = {
    'derives' : 'bottom', 'cls_short' : 'b',
    'new': [1, function(v){ // Very limited definition of false here.
        return [(v===false  || v===null || v===undefined) ? false :true]
    }],

    '!' : [1, function(b) { return [!b] }],
    '|' : [2, function(a,b) { return [a || b] }],
    '&' : [2, function(a,b) { return [a && b] }],
    'q' : [3, function(t,f,q) { return [q ? t : f] }],

    '?' : ['cm', "qLpR"],

    // Break one level.  // NOTE: kindah limited..
    'B' : ['m', function(vars, chvals, clses, stack, code, i) {
        if( stack.pop()===true ) {
            return code.length
        }
        return i
    }],
}
default_clses['null'] = {
    'derives':'bottom', 'new':[0, function(){ return null }]
}
default_clses['undefined'] = {
    'derives':'bottom', 'new':[0, function(){ return undefined }]
}
