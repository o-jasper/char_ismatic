
default_clses['intfeed'] = {
    'no_val' : true,
    'cls_short' : 'if',
    'as' : 'o',

    'exit' : "[i]f.",

    'default' : ['m', function(vars, chvals, clses, stack, code, i) {
        var e = stack.splice(-2,2)
        var d = e[1].charCodeAt() - 48
        if( d>=0 && d<=9 ){
            e[0].i = e[0].i*10 + d
            stack.push(e[0])
            return i
        } else if( e[1]=='.' ) { // Floating point instead.
            stack.push({'tp':'ff', 'x':el[1].i, 'div':1e1})
            return i
        } else {
            stack.push(e[0].i)  // Integer feeding ended.
            // This why it is a macro; need to still interpret the character.
            return i - 1
        }
    }],
}
default_clses['floatfeed'] = {
    'no_val' : true,
    'cls_short' : 'ff',
    'as' : 'o',
    'exit' : "[x]f.",
    'default' : ['m', function(vars, chvals, clses, stack, code, i) {
        var e = stack.splice(-2,2)
        var d = e[1].charCodeAt() - 48
        if( d>=0 && d<=9 ){
            e[0].x = e[0].x + d/e[0].div
            e[0].div *= 10
            stack.push(e[0])
            return i
        } else {
            stack.push(e[0].x)  // Float feeding ended.
            return i - 1
        }
    }]
}
