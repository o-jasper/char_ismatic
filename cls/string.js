
default_clses['string'] = {
    'derives' : 'bottom', 'cls_short' : 's',
    'new' : "[]",
    // Read the stack into bunch of variables.
    '^' : ['m', function(vars, chvals, clses, stack, code, i) {
        var s = stack.pop()
        var repush= [], endindices = {}
        for( var j=s.length-1 ; j >=0 ; j-- ) {
            var ch = s[j]
            switch(ch) {
                case '_': stack.pop(); break  // Drop it.
                case '.': repush.push(stack.pop()); break // Keep it.
                // End indices, highest will be last, undefineds bug you if not
                //  complete.
                case '0': case '1': case '2': case '3': case '4':
                case '5': case '6': case '7': case '8': case '9':
                    endindices[ch] = stack.pop(); break
                default:  chvals[ch] = stack.pop(); break // Into var.
            }
        }
        while(repush.length>0){ stack.push(repush.shift()) }

        var n_ind = Object.keys(endindices).length
        for( var j=0 ; j<n_ind ; j++ ) {
            stack.push(endindices['' + j])
        }
        return i
    }], // Run the code, in same context.

    'R' : "[C]tn",  // Make a code object from it.

    // Length of string.
    '#' : [1, function(s){ return [s.length] }],
    // Substring.
    's' : [3, function(f,l,s){ return [s.substr(f, l)] }],
    // As number; float/integer.
    'n' : [1, function(s){ return [parseFloat(s)] }],
    'x' : [1, function(s){ return [parseInt(s, 16)] }],  // ..hex

    // TODO regex object from it.
    //'r' : [3, function(e){ return [e[0].replace('' + e[1], e[2])] }],

    // Produce errors.
    'E' : [1, function(s){ console.error(s); return [] }],
    'L' : [1, function(s){ console.log(s); return [] }],

    'i' : [1, function(s){    return [s.charCodeAt()] }],
    '.' : [2, function(i, s){ return [s[i]] }],
    'A' : [2, function(a,b) { return [a + b] }],
    '=' : [2, function(a,b) { return [a==b] }],
}
