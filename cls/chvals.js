
default_clses['chvals'] = {
    'derives' : 'bottom',
    // Class of the local variables, it can be accessed with `L`.
    'as' : 'o',
    'p' : "[c]^[parent]c.[tp,chvals]csd", // Alter to copy of parent.
    'r' : ['m', function(vars, _chvals, clses, stack, code, i) {
        var chvals = stack.pop()
        if( chvals.tp != 'chvals' ){ console.error("BUG") }
        var body = code_string(stack.pop())
        stack.push(raw_run(vars, chvals, clses, stack, body))
        return i
    }],
    'R' : "[x]^[tp,chvals]xsrd",
}
