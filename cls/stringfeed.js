
// Stringfeeds/escape dont derive from bottom, they're pretty much parsing.
default_clses['stringfeed'] = {
    'no_val' : true,
    'cls_short' : 'sf',
    'default' : [2, function(sf, c) {  // Append character.
        sf.s += c
        return [sf]
    }],
    '\\' : [1, function(sf) { // Escape one character.
        if( sf.d>0 ){ sf.s += '\\' }
        sf.tp='ef'
        return [sf]
    }],
    '[' : [1, function(sf) { // Increase depth.
        sf.d += 1
        sf.s += '['
        return [sf]
    }],
    ']' : [1, function(sf) {  // Lower depth.
        sf.d -= 1
        if( sf.d >=0 ) {
            sf.s += ']'
            return [sf]
        } else {
            if( sf.t == '{' ){ console.error("Code closed wrong", JSON.stringify(sf)) }
            return [sf.s]  // At point to return.
        }
    }],

    '{' : [1, function(sf) { // Increase depth.
        sf.d += 1
        sf.s += '{'
        return [sf]
    }],
    '}' : [1, function(sf) {  // Lower depth.
        sf.d -= 1
        if( sf.d >=0 ) {
            sf.s += '}'
            return [sf]
        } else {
            if( sf.t != '{' ){ console.error("String closed wrong", JSON.stringify(sf)) }
            sf.tp = 'C'  // Change to code type.
            return [sf]
        }
    }],
    // Start another string.(if ontop & not code)
    ',' : [1, function(sf) {
        if( sf.d == 0 && sf.t!='{' ) {
            return [sf.s, {'tp':'sf', 'd':0, 's':""}]
        } else {
            sf.s += ','
            return [sf]
        }
    }]
}

default_clses['escapefeed'] = {
    'cls_short' : 'ef',
    'default' : [2, function(ef, ch) { ef.s += ch; ef.tp='sf'; return [ef] }],
}
