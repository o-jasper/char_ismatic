# A char\_acteristic programming language
It does not have a parser, it just reads characters.

In `html/pixelfun` you can play with.. pixels with this.

NOTE/TODO it is barely tested.

### Description
It is a stack language, every next character either pushes a corresponding
local variable, or calls the corresponding method of the item on top of the stack.

If no method is found, it puts a character ontop of the stack and calls the
`default` method for that class.

There is also the `exit` method, and a `new` method.

Classes can derive from one other class.

**How does it read values?** for integer, any integer `0-9` on the "bottom class"
you start with, puts an integer feeder instance on. That feeder has a `default`
method that interprets integer-characters. It also interprets `.` as meaning to
convert to a floating-point feeder.

There is similarly a string feeder on `[` and ends on `]`. Actually, keeps track
of depth so it is easy to nest if it is code. For convenience `,` starts a new
list if at zero depth. `\` or course escapes.

Most things ultimately derive from the bottom class, objects have to take care
to leave enough free.

*For instance* `G` accesses global variables there, `[someclass]t` accesses
`someclass`. Internally `t` is defined as `[cls]G..`.
(push to stack string `cls`, push globals, access (globals)dictionary,
then the top of the stack is your string, and the classes, so the class is
acccessed.) A class has a class too, it is `class` or `c`. It has the method
`n` to create a new one.

**Is there an API yet?** not yet. Definitely TODO if i am to take this unseriously
properly.

**Is there a DUP?** Now there is, it is `B`. (in `bottom` class)
still often better to use `^` for accessing stuff.

*For instance* `[xyz]^` takes three values of the stack and puts them in `x`, `y`,
`z` respectively.

### Interesting aspects
Hard to read, but ultra short.

No distinction between methods and functions, or namespaces and classes.

Global variables dont really exist, they're just the `G` method output on the
bottom class.

Looping is done without variable, just pushing things onto the stack for each
round.

### TODO / things that could be done, i guess.
Above comment on global variables not yet quite reality.

More things could be written in the language itself.

Pretty sure with minor modifications it is possible to provide multi-character
variable/method accessing.

Run in a way that figures out all the constants and creates a new runnable object.

*Related* "type" run, run with types, have alternate methods that just say what
output types would be put on the stack.
