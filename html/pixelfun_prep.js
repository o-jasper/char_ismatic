
var glob_vars = {
    'code':run_code,
    'examples' : examples,
    'pix':pix, 'width':width, 'height':height,
    'cls':turtle_cls
}
state = new_state({ 'vars':glob_vars })
run(state, "[commentfeed,code]G..RR")

console.log(state)

gui_run_continuous(200)
