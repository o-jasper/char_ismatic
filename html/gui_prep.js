
cmd_hist = JSON.parse(localStorage.getItem(localstorage_name) || "{}")

for( var i=0 ; i<cmd_hist.n ; i++ ) {
    var cur = cmd_hist['' + i]
    if( cur ) { show_entry(cur[0], cur[1], i) }
}
URL = document.URL
if( URL.search("s=") >=0 ) {
    ge('input').value = decodeURIComponent(URL.substr(URL.search("s=")+2))
    gui_run()
}

ge('run').onclick = gui_run
ge('clear').onclick = gui_clear
ge('input').onkeydown = gui_key
