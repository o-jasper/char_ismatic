//  Copyright (C) 12-05-2019 Jasper den Ouden.
///
//  This is free software: you can redistribute it and/or modify
//  it under the terms of the Affero GNU General Public License as published
//  by the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.

width = 800, height = 800

ctx = ge('canvas').getContext('2d')
imgData = ctx.createImageData(width, height)
pix = imgData.data
for (var i = 0; i < pix.length; i ++) { pix[i] = 255; }

function draw_it() { 
    ctx.putImageData(imgData, 10, 10)  // No idea what the ten stands for.
}

var index_modulo = 4*width*height
function i_of_xy(x,y) {  // Index of x,y, negative/overly high values wrap.
    var r = 4*(x + width * y)
    while( r < 0 ){ r += 2*index_modulo }
    return r%index_modulo
}
planned = []
planned_cnt = 0, did_cnt = 0
function raw_register(turtle, dt) {  // Register a turtle.
    while(planned.length <= dt){ planned.push([]) }
    if( dt >= planned.length ){ console.error("huh", dt, planned) }
    planned[dt].push(turtle)
    planned_cnt += 1
}

turtle_cls = {}

turtle_cls['pos'] = {
    'derives':'l',
    'new' : [2, function(x,y) {
        return [[x,y, 4*(y*width + x)]]
    }],
    // Access colors.
    'r' : [1, function(p){ return [pix[p[2] + 0]] }],
    'g' : [1, function(p){ return [pix[p[2] + 1]] }],
    'b' : [1, function(p){ return [pix[p[2] + 2]] }],
    'a' : [1, function(p){ return [pix[p[2] + 3]] }],
    // Set colors.
    'R' : [2, function(v, p){ pix[p[2] + 0] = v; return [p] }],
    'G' : [2, function(v, p){ pix[p[2] + 1] = v; return [p] }],
    'B' : [2, function(v, p){ pix[p[2] + 2] = v; return [p] }],
    'A' : [2, function(v, p){ pix[p[2] + 3] = v; return [p] }],

    '.' : "[v]^vrvgvbva",  // Put/set whole color.
    's' : "ABGR"
}
turtle_cls['turtle'] = {
    'derives':'o',
//    'new':
    'p' : turtle_cls['pos']['new'],  // ("[pos]tn") Decided want performance here.
    'r' : [2, function(dt, turtle){ raw_register(turtle, dt); return [] }]
}
for( var k in default_clses ){ turtle_cls[k] = default_clses[k] }

function run_1() { // One run step.
    var lst = planned.shift()
    if( lst ) {
        planned_cnt -= lst.length
        did_cnt     += lst.length
        for( var i=0 ; i<lst.length ; i++ ) {
            var turtle = lst[i]
            // TODO if i want to evolving algos, it'll definitely be able to mess
            //  around, screwing up the state..
            run(state, "R", [{'tp':'B'}, turtle])  // Run the turtle.
        }
    }
}

function gui_run_continuous(interval) {
    console.log('started loop')
    setInterval(function() {
        run_1()
        draw_it()
        ge('did_cnt').textContent     = did_cnt
        ge('planned_cnt').textContent = did_cnt
    }, interval)
}
