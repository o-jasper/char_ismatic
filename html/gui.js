
localstorage_name = null
{
    var x = location.href.split('/')
    x = x[x.length-1]
    localstorage_name = x
}

function save_hist() {
    localStorage.setItem(localstorage_name, JSON.stringify(cmd_hist))
}

function gui_cpy(i) {
    var prev = ge('input').value.trim()
    if(prev!=""){ prev += "\n" }
    ge('input').value = prev + cmd_hist['' + i][0].trim()
    ge('input').focus()
}
function gui_run_hist(i) {  // TODO how the fuck is it adding to the history.
    gui_run('dry')
    gui_cpy(i)
}
function gui_rem(i) {
    cmd_hist['' + i] = undefined
    save_hist()
    var rem_el = event.target.parentElement.parentElement
    rem_el.parentElement.removeChild(rem_el)
}

function gui_replace(i) {
    gui_cpy(i)
    gui_rem(i)
    ge('input').focus()
}

entry_i = 0
function show_entry(code, result, n) {
    entry_i += 1
    var i = entry_i
    var h = "<div i='" + n + "'><span class='code' id='c_" + i + "'></span>"
    h += "<button id='cpy_" + i + "'>C</button>"
    h += "<button id='run_" + i + "'>R</button>"
    h += "<button id='rem_" + i + "'>D</button></div>"
    h += "<div class='result' id='r_" + i + "'></div>"

    var el = document.createElement('div')
    el.className = 'hist_el'
    el.innerHTML = h
    ge('result').appendChild(el)

    ge('c_' + i).textContent = code
    ge('r_' + i).textContent = JSON.stringify(result)

    ge('cpy_' + i).onclick = function(){
        gui_cpy(this.parentNode.getAttribute('i'))
    }
    ge('run_' + i).onclick = function(){
        gui_replace(this.parentNode.getAttribute('i'))
    }
    ge('rem_' + i).onclick = function(){
        gui_rem(this.parentNode.getAttribute('i'))
    }
}
function add_entry(code, result) {
    show_entry(code, result, cmd_hist.n)

    cmd_hist['' + cmd_hist.n] = [code, result, {}]
    cmd_hist.n++
    save_hist()
}

function gui_run(dry) {
    var code = ge('input').value
    if( code != "" ) {
        var ret = "(not run)"
        if( state.chvals.tp != 'chvals' ){ console.error('BUG') }
        if( dry!='dry' ){ ret = run(state, code) }

        // TODO will want keybindings to do stuff with result.
        add_entry(code, ret)
        ge('input').value = ""
    }
}

function gui_key() {
    if(event.keyCode == 13 && event.ctrlKey) {
        event.preventDefault()
        gui_run()
    }
}

function gui_clear() {
    localStorage.setItem(localstorage_name, JSON.stringify({'n':0}))
    ge('result').textContent = ""
}
