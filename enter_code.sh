#!/usr/bin/bash

echo "
$1 = {}"

while read file; do
    NAME=$(basename $file|cut -f 1 -d .)
    echo "$1['$NAME'] = \"\"";
    cat $file | while read line; do
        echo "+ \"$line\\n\""
    done;
    echo
done
