# The makefile only does tests.

default: test html/examples.js

test: test.js

test/result/:
	mkdir test/result

test.js: test/result/
	make test/result/test.js ; diff test/result/test.js test/shouldbe/test.js

test/result/test.js: Makefile test/test.js char_ismatic.js cls/*js util/*.js enter_code.js test/enter_code.js
	cat char_ismatic.js cls/number.js cls/*.js util/no_browser.js enter_code.js test/enter_code.js test/test.js | js24 /dev/stdin \
		> test/result/test.js

enter_code.js: enter_code.sh ch_/*.ch_
	ls ch_/*.ch_ | sh enter_code.sh run_code > enter_code.js

test/enter_code.js: enter_code.sh test/ch_/*.ch_
	ls test/ch_/*.ch_ | sh enter_code.sh run_test_code > test/enter_code.js

html/examples.js: enter_code.sh html/ch_/*.ch_
	ls html/ch_/*.ch_ | sh enter_code.sh examples > html/examples.js
